<?php

//Composer bootstarp
require __DIR__ . '../vendor/autoload.php';

// Native php autoloading implémentation : mynamspace\myclass.php
set_include_path(realpath('../src') . PATH_SEPARATOR .  get_include_path() );
spl_autoload_extensions('.php');
spl_autoload_register();


function ConfigPath($url) {
    return define('RELATIVE_BASE_URL', $url) && define('ABSOLUTE_BASE_URL', (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . RELATIVE_BASE_URL);
}


ConfigPath('/domus/htdocs/');


$frontControler = new \fr\webetplus\rootedit\core\RootEdit();

$frontControler->container->set('dataSource', '\fr\webetplus\rootedit\persistance\MySQLPDO', 'domus', 'root', '', 'localhost');
$frontControler->container->set('ecritureRepository', '\persistance\EcritureRepository', 'dataSource');
$frontControler->container->set('AccountRepository', '\persistance\AccountRepository', 'dataSource');
$frontControler->container->set('ajouterEcriture', '\domus\AddEntry');



//$frontControler->skinurl = realpath(__DIR__ );
//$frontControler->addTemplate('main', '#', '/view/index.phtml');
$frontControler->addRoute('ecriture/ajout/', 'ajoutEcriture');
$frontControler->addRoute('ecriture/', 'main&ecriture');
$frontControler->addRoute('livre/', 'main&livre');
$frontControler->addRoute('prevision/', 'main&prevision');
$frontControler->addRoute('/', 'main&index');


$frontControler->addTemplate('main', '#', '/view/index.phtml');
$frontControler->addTemplate('livre', 'main', '/view/livre.phtml');
$frontControler->addTemplate('ecriture', 'main', '/view/ecriture.phtml');


$frontControler->addAction('main', function($in, $out, $front) {
    $out->header = "<h1>Header</h1>";
    $out->footer = "<h1>FOOTER</h1>";
});
$frontControler->addAction('index', function($in, $out, $front) {
    //$frontControler->container->listerCompte->
    $out->main = "<h1>Contenue :p</h1>";
});
$frontControler->addAction('livre', function($in, $out, $front) {
    // $frontControler->container->listerCompte->
});
$frontControler->addAction('ecriture', function($in, $out, $front) {
    
});
$frontControler->addAction('ajoutEcriture', function($in, $out, $front) {
    $front->container->ajouterEcriture->add($in->number('idCompteDebit'), $in->number('idCompteCredit'), $in->number('montant'));
});
try {
    
    //l'idée est d'encapsuler l'output le httpoutput dans l'objet view pour permettre une encapsulation de la stream body 
    //et de ne faire les include que au dernier moments et en direct ....
    //soit la class view implement l'interface ResponsseInterface... soit c'est la classe output qui implemente l'interface de view ...
    
    //apopors de la carte :: prévoir un moyen pour les template de gérer le xml, le pdf, le json etc..
    
    //voire si on veux output du html , ou du pdf ou du xml dans reponsse->getBody()->write(json_encode($data))
    //ou alors directement ecrir dans view genre $frontControler->container->view->json = json_encode($data)
    
    //gaffe pour view si url de la racine  n'as pas été définit ca plante .... 
    
    //apropos de la carte :: Ajouter la gestion des "HTTP Middlewares" au format PSR-15
//    décider si on passe la vue en deuxième param aux commande ???
//    ou alors la vue View reste dans container ?
//    et comment on affiche a la fin ? on utilise la stream de la reponsse générer par start ??? ?
    
     echo $frontControler->start()->getBody()->getContents();
   // $frontControler->start(); 
   // $frontControler->view()->render();
    
    
} catch (\Exception $exc) {
    echo $exc->getMessage();
} finally {
    
}

 